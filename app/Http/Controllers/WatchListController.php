<?php

namespace App\Http\Controllers;

use App\Http\Requests\WatchListRequest;
use App\WatchList;
use App\Helpers\TMDBHelper;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WatchListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {

        if ($request->ajax()) {
            $movieIds = (new WatchList)->where('user_id', '=', Auth::id())->get()->pluck('movie_id');
            $movies = (new TMDBHelper)->findMoviesByIds($movieIds);
            return response()->json(['watchLater' => $movieIds, 'movies' => $movies]);
        }

        return view('app');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WatchListRequest $request)
    {
        $exists = Auth::user()
            ->watchlist()
            ->where('movie_id', $request->get('movie_id'))
            ->count();

        if (!$exists) {
            Auth::user()->watchlist()->create(['movie_id' => $request->get('movie_id')]);
        }

        return response()->json(['message' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WatchList  $watchList
     * @return \Illuminate\Http\Response
     */
    public function destroy($movie_id)
    {
        Auth::user()->watchlist()->where('movie_id', $movie_id)->delete();

        return response()->json(['message' => 'success']);
    }
}
