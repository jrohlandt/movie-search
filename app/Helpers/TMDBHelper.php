<?php

namespace App\Helpers;

use Illuminate\Support\Facades\App;

class TMDBHelper {

    private $api_url;
    private $api_key;

    public function __construct()
    {
        $this->api_url = 'https://api.themoviedb.org/3/';
        $this->api_key = config('themoviedatabase.api_key');
    }


    public function fetchPopular()
    {
        // https://api.themoviedb.org/3/movie/popular?api_key=531eaffcac14a8c431f91d7a77a345e8
        return $this->curl('get', "movie/popular")['results'];
    }

    public function searchByTitle($title)
    {
        // https://api.themoviedb.org/3/search/movie?api_key=531eaffcac14a8c431f91d7a77a345e8&query=home%20alone
        $title = urlencode($title);
        return $this->curl('get', "search/movie?query={$title}")['results'];
    }

    public function findMoviesByIds($ids)
    {
        if (empty($ids)) return [];

        // there is no way in v3 of tmdb api to fetch many movies at once (not sure about other versions) // todo need to investigate further.
        $movies = [];
        foreach ($ids as $id) {
            $movie = $this->curl('get', "movie/{$id}");
            if (!empty($movie)) $movies[] = $movie;
        }

        return $movies;
    }


    protected function curl($method, $path, $data=[], $headers=[])
    {
        if (!in_array(strtolower($method), ['get', 'post', 'patch', 'put', 'delete'])) {
            return ['message' => 'CURL error: Invalid HTTP method ' . $method];
        }

        //Set up API path and method
        $url = $this->api_url . $path;
        $q = strpos($url, '?') ? '&' : '?';
        $url .=  "{$q}api_key={$this->api_key}";

        //Create request data string
        $data = http_build_query($data);

        //Execute cURL request
        $ch = curl_init();
//        if (in_array(strtolower($method), ['post', 'patch', 'put', 'delete'])) {
//            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($method));
//        } else {
//            $url = $url . "?" . $data;
//        }

//        if (App::environment('local') || App::environment('testing')) {
//            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
//            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
//        }

        curl_setopt($ch, CURLOPT_URL, $url);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, [
//            "Authorization: Bearer {$this->api_key}",
//            "X-Requested-With: XMLHttpRequest",
//        ]);
        // make sure curl returns a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);

        if ($output === false) {
            return ['error' => curl_error($ch)];
        }
        $httpResponseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $response = json_decode($output, true);
        if (isset($response['error']) && $response['error'] === 'Unauthenticated.' ) {
            $response['message'] = 'not_authenticated';
        }
        $response['status'] = $httpResponseCode;
        return $response;
    }
}