<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::middleware(['auth'])->group(function() {
    Route::get('/', 'MovieController@index')->name('movies');
    Route::get('/movies', 'MovieController@index')->name('movies');
    Route::get('/movies/popular', 'MovieController@popular')->name('movies.popular');
    Route::post('/movies/search', 'MovieController@search')->name('movies.search');
    Route::get('/watch-later', 'WatchListController@index')->name('watch-later');
    Route::get('/watchlist', 'WatchListController@index')->name('watchlist');
    Route::post('/watchlist', 'WatchListController@store')->name('watchlist.store');
    Route::delete('/watchlist/{movie_id}', 'WatchListController@destroy')->name('watchlist.delete');

    // todo logout

});

