Stack:

Laravel 5.8, Reactjs

This is a standard Laravel app and the react code is already built, just install it like a normal laravel app 
and don't forget to run artisan key:generate, artisan migrate and artisan db:seed .
Add db details to .env file also add tmdb api key to .env file (see .env.example).
And then you can sign in with demo@example.com, password 123456
I'm guessing you are probably just going to be looking at the code anyway but please let me know if you need to run the app and if you have any trouble.

The source code you will be interested in can be found in:
Laravel/PHP: app/Http/Controllers app/Helpers/TMDBAPIHelper.php
React: resources/app/src

I made loads of compromises:
 * no real error handling
 * no unit or feature testing
 * used laravel's mix package for transpilation of react code 
 * used inheritance in react app, I tried composition but ended up wasting too much time and had to move on.
 * didn't really think about naming or directory structure in React app
 * didn't use prop-types
 * Could have created a autocomplete dropdown for the search input that would have allowed the
 user to add movies to the watch list right from the search component.
 But I wasn't sure if there is a limitation on the TMDB api so I thought rather not hit the api for every key stroke.
  Instead I created a MovieList component that I re-used on the search page and on the watch-later page.
 With some tweaks I can probably still re-use the MovieList component in a dropdown..
 * didn't add user logout.
 * pre-built react code and saved to version control (so you don't need install npm deps and build with mix.)
 * used php's built in curl functions instead of Guzzle.
 * many more...
 